#!/usr/bin/env python

# This is CopyCat 0.1.1 - NonStable
# A tool made for utilitarian humans by @KartSriv

"<----------------------------Modules---------------------------->"
import os # For system commands
import sys # For beginning statement
import io # Writing Files
import subprocess # Storing Outputs
"<----------------------------Modules---------------------------->"

"<---------------------------Functions--------------------------->"
def Correpted():
    print "OOPS! Program Correpted, mate!"
    print "ERROR #003"
    sys.exit() # Exit

def isFile(FileName):
    Answer = os.path.isfile(FileName)
    return Answer

def FileExist(FileName):
    Answer = os.path.exists(FileName)
    return Answer

def ValidateCopy(File):
    File_SUBPROCESS = File.replace(' ', '\ ')
    FilePath = subprocess.check_output("realpath " + File_SUBPROCESS, shell=True) # Full Copy Path
    FileDirectory = os.path.dirname(FilePath)
    if os.path.exists(FileDirectory) == False:
        print "OOPS! This directory does not exist, mate!"
        print "ERROR #003"
        sys.exit()
    os.chdir(FileDirectory)
    if os.path.exists(File) == False:
        print "OOPS! This File does not exist, mate!"
        print "ERROR #004"
        sys.exit() # Exit
    if os.path.isdir(File) == True:
	print "OOPS! This is a Directory, mate!"
        print "ERROR #005"
        sys.exit() # Exit

"<---------------------------Functions--------------------------->"

"<-----------------------------VARS------------------------------>"
HowManyArg = len(sys.argv)
"<-----------------------------VARS------------------------------>"

"<-----------------------------Code------------------------------>"
if HowManyArg == 1:
    print "OOPS! You got no options over here, mate!"
    print "ERROR #001"
    sys.exit() # Exit
elif HowManyArg == 2:
    CopyLocation = sys.argv[1]
    #FullCopyFilename = subprocess.check_output("realpath " + CopyLocation, shell=True) # Full Copy Path
    #os.remove("ClipboardCurrent.txt") # Remove ClipboardCurrent
    #with io.FileIO("ClipboardCurrent.txt", "w") as file:
        #file.write(CopyLocation)
    #print "Only Copy"
    ValidateCopy(CopyLocation)
    print "Copy"
    sys.exit() # Exit
elif HowManyArg == 3:
    print "Copy and Paste"
    sys.exit() # Exit
elif HowManyArg > 3:
    print "OOPS! Lot of options over there, mate!"
    print "ERROR #002"
    sys.exit() # Exit
else:
    Correpted()

"<-----------------------------Code------------------------------>"

"___________________________END OF CODE___________________________"
