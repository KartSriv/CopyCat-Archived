# CopyCat
**PROJECT ARCHIVED**: 
https://kartsriv.github.io/CopyCat/
> Tired of "cp" which doesn't have a damn clipboard then it's your lucky day mate!<br>
<br>
  <a href="https://saythanks.io/to/KartSriv">
      <img src="https://img.shields.io/badge/SayThanks.io-%E2%98%BC-1EAEDB.svg">
  </a>

[![forthebadge made-with-python](http://ForTheBadge.com/images/badges/made-with-python.svg)](https://www.python.org/)
[![ForTheBadge built-with-science](http://ForTheBadge.com/images/badges/built-with-science.svg)](https://github.com/KartSriv/)
[![ForTheBadge built-with-love](http://ForTheBadge.com/images/badges/built-with-love.svg)](https://github.com/KartSriv/)



If you have ever used a **GUI OS** then you'll know what's COPY-PASTE:bookmark:. *This is a very useful:sunglasses: tool*.
How it works is you **copy** one file and **paste** it on somewhere else(**We all know that and we're not from 300 BC**)
But **Linux Users** use a command called '**cp**' where we need to use it as:

```
$ cp /home/user/folder1/file.txt /home/user/folder2/
```
but when you don't know where to paste the file(Didn't create the folder) or you want to paste it in many directories:
You might need something like this. If you didn't understand the use of this program see the short story below.
```
 $ mkdir /home/user/folder2/ # Create a folder 
 $ cp  /home/user/folder1/file.txt /home/user/folder2/ 
 $ cd blah/blah 
 $ python blah.py # Do something 
```
*1) SHIT! I want to paste that file again <br />
2) HOLY SHIT! I forgot the path! Let's use the command history <br />
3) UP ARROW - Million Times <br />
4) Found it: ``` cp /home/user/folder1/file.txt /home/user/folder2/ ``` <br />
5) Modify command: ```cp /home/user/folder1/file.txt /home/user/blah/blah/``` <br />
6) TIME WASTED: 10 Minutes:trollface: <br />*

(OR)

```
 $ copy /home/user/folder1/file.txt # Copied a file
 $ mkdir /home/user/folder2/ # Create a folder
 # SHIT! I want to paste that file again 
 # Let's paste it!
 $ paste /home/user/folder2/ # Pasted at new folder 
 $ cp  /home/user/folder1/file.txt /home/user/folder2/ 
 $ cd blah/blah 
 $ paste /home/user/folder3/ # Pasted again at a different folder
```
I have created COPY-PASTE for Linux 
