#!/usr/bin/env python

# This is EfficientCopy 3.2.1
# A utility in Synergy by @karthiksrivijay

# Modules
import os # For system commands
import sys # For beginning statement
import subprocess # Storing Outputs
import pathlib # Check File Exists
import os.path # Check If File is File

def Correpted():
    print "OOPS! Program Correpted, mate!"
    print "ERROR #003"
    exit

def isFile(FileName):
    Answer = os.path.isfile(FileName)
    return Answer

def FileExist(FileName):
    Answer = os.path.exists(FileName)
    return Answer

def ValidateCopy(Copy_Location):
    FullCopyFilename = subprocess.check_output("realpath " + Copy_Location, shell=True) # Full Copy Path
    if isFile(FullCopyFilename) == False:
        print "OOPS! This is not a file, mate!"
        print "ERROR #004
        exit
    elif isFile(FullCopyFilename) == True:
        if FileExist(FullCopyFilename) == True:
            Answer = "Yes"
        elif FileExist(FullCopyFilename) == False:
            print "OOPS! This File does not exist, mate!"
            print "ERROR #005"
            exit
        else:
            Correpted()
    else:
        Correpted()
    return Answer

def ValidatePaste(Copy_Location,Paste_Location):
    # Copy
    FullCopyFilename = subprocess.check_output("realpath " + Copy_Location, shell=True) # Full Copy Path
    if isFile(FullCopyFilename) == False:
        print "OOPS! This is not a file(Copy), mate!"
        print "ERROR #006"
        exit
    elif isFile(FullCopyFilename) == True:
        if FileExist(FullCopyFilename) == True:
            Answer2 = "Yes"
        elif FileExist(FullCopyFilename) == False:
            print "OOPS! This File does not exist(Copy), mate!"
            print "ERROR #007"
            exit
        else:
            Correpted()
    else:
        Correpted()
    # Paste
    FullPasteFilename = subprocess.check_output("realpath " + Paste_Location, shell=True) # Full Paste Path
    if isFile(FullPasteFilename) == False:
        print "OOPS! This is not a file(Paste), mate!"
        print "ERROR #008"
        exit
    elif isFile(FullPasteFilename) == True:
        if FileExist(FullPasteFilename) == True:
            Answer2 = "Yes"
        elif FileExist(FullPasteFilename) == False:
            print "OOPS! This Paste File does not exist(Paste), mate!"
            print "ERROR #009"
            exit
        else:
            Correpted()
    else:
        Correpted()
    if Answer1 == "Yes" and Answer2 == "Yes":
        Answer = "Yes"
    else:
        Correpted()
    return Answer


# ERROR Check for number of Arguments
if len(sys.argv) > 3: # Too Many Arguments
    print "OOPS! Lot of options over there, mate!"
    print "ERROR #001"
    exit
elif len(sys.argv) <= 1: # No Argument
    print "OOPS! You've got no options over here, mate!"
    print "ERROR #002"
    exit
elif len(sys.argv) < 1: # Correpted
    Correpted()

# Extract File Name and Location
#Full_FileName = subprocess.check_output("realpath " + Copy_FileName, shell=True) # Full Path
#FileName_Re = subprocess.check_output("basename " + Full_FileName, shell=True) # Only FileName

# Copy or Copy Paste Int
if len(sys.argv) == 2: # Copy Int
    Copy_Location = sys.argv[2]
    if ValidateCopy(Copy_Location) == "Yes":

elif len(sys.argv) == 3: # Copy Paste Int
    Copy_Location = sys.argv[2]
    Paste_Location = sys.argv[3]
    if ValidatePaste(Copy_Location,Paste_Location) == "Yes":
        os.system("cp "+Copy_Location+" "+Paste_Location)
    else:
        Correpted ()

else:
    Correpted()
