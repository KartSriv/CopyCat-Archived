#!/usr/bin/env python

# This is FastCopy 1.1
# A utility in Synergy by @karthiksrivijay

# Modules
import os # For system commands
import sys # For beginning statement


# Taking the user input from statement
# For more information check: https://stackoverflow.com/questions/44024686/how-to-take-input-in-beginning-of-the-program-in-python

Source = sys.argv[1] # Using sys Modules
Destination = sys.argv[2] # Using sys Modules

# Doing a copy with os.system()
# For more information check: https://stackoverflow.com/questions/15867952/python-3-2-os-module-python-functions-or-linux-commands

os.system("cp -R "+Source+" "+Destination) # Using os module and system function
